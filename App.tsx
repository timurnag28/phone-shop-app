import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';

import {GestureHandlerRootView} from 'react-native-gesture-handler';
import AppNavigator from './src/navigation/AppNavigator.tsx';
import FlashMessage from 'react-native-flash-message';

function App() {
  return (
    <GestureHandlerRootView>
      <SafeAreaView style={[{flex: 1}]}>
        <StatusBar />
        <AppNavigator />
        <FlashMessage position="top" />
      </SafeAreaView>
    </GestureHandlerRootView>
  );
}
export default App;
