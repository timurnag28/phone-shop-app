import {makeAutoObservable, runInAction} from 'mobx';
import {Api} from '../services/Api.ts';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {ParamListBase} from '@react-navigation/native';
import {editableProductFields} from '../share/FormFields.ts';
import Utils from '../share/Utils.ts';
import {CHANGES_CONFIRMED, DANGER, SUCCESS} from '../share/consts.ts';
import {BaseResponse, Field, ProductItem} from '../inerfaces/interfaces.ts';

class ProductsStore {
  constructor() {
    makeAutoObservable(this);
  }

  productList: ProductItem[] = [];
  filteredProductList: ProductItem[] = [];
  currentProduct: ProductItem | null = null;
  editableProductFields: Field[] = editableProductFields;
  editableProduct: ProductItem = {
    id: 1,
    SKU: '',
    name: '',
    description: '',
    image: '',
    price: '',
  };
  searchText: string = '';
  isBottomSheetActive: boolean = false;
  isLoading: boolean = true;
  isSomeFieldHasChanged: boolean = false;

  onGetProducts = () => {
    this.isLoading = true;
    new Api()
      .onGetProducts()
      .then((response: BaseResponse<ProductItem[]>) => {
        runInAction(() => {
          this.productList = response.data;
          this.filteredProductList = this.productList;
          this.isLoading = false;
        });
      })
      .catch((errorMessage: string) => {
        this.isLoading = false;
        console.log(errorMessage);
      });
  };

  setSearchText = (text: string) => {
    this.searchText = text;
  };

  onSearchProduct = (searchableText: string) => {
    if (searchableText) {
      this.filteredProductList = this.productList.filter(productItem =>
        productItem.name.toLowerCase().includes(searchableText.toLowerCase()),
      );
    } else {
      this.filteredProductList = this.productList;
    }
  };

  onCancelSearch = () => {
    this.searchText = '';
  };

  setCurrentProduct = (item: ProductItem) => {
    this.currentProduct = item;
  };

  setBottomSheetActive = (value: boolean) => {
    this.isBottomSheetActive = value;
  };

  onFillEditableProductFields = (productItem: ProductItem) => {
    runInAction(() => {
      this.editableProduct = productItem;
      this.editableProductFields.map(item => {
        item.value = productItem[item.key];
      });
    });
  };

  setEditableProductField = (index: number, value: string) => {
    runInAction(() => {
      this.editableProductFields[index].value = value;
      if (!this.isSomeFieldHasChanged) {
        this.fieldHasChangedByUser(true);
      }
    });
  };

  fieldHasChangedByUser = (value: boolean) => {
    this.isSomeFieldHasChanged = value;
  };

  onConfirmUpdateProduct = (
    navigation: NativeStackNavigationProp<ParamListBase>,
  ) => {
    const error = Utils.onCheckFields(this.editableProductFields);
    if (error) {
      return Utils.onShowMessage(error, DANGER);
    }
    let fieldsHaveUpdated = this.onCheckFieldsUpdated();
    if (!fieldsHaveUpdated) {
      return navigation.goBack();
    }

    this.isLoading = true;
    let newProduct = this.autoGenerateNewProduct();
    new Api()
      .onUpdateProduct(newProduct)
      .then((response: BaseResponse<ProductItem>) => {
        this.autoUpdateProduct(response.data);
        if (navigation.canGoBack()) {
          navigation.goBack();
        }
      })
      .catch((errorMessage: string) => {
        this.isLoading = false;
        console.log(errorMessage);
      });
  };

  onCheckFieldsUpdated = () => {
    return this.editableProductFields.find(
      item => this.editableProduct[item.key] !== item.value,
    );
  };

  autoGenerateNewProduct = () => {
    let newProduct: ProductItem = {
      id: this.editableProduct.id,
      SKU: '',
      name: '',
      description: '',
      image: this.editableProduct.image,
      price: '',
    };

    runInAction(() => {
      this.editableProductFields.map(field => {
        newProduct[field.key] = field.value;
      });
      this.isSomeFieldHasChanged = false;
    });

    return newProduct;
  };

  autoUpdateProduct = (updatedProduct: ProductItem) => {
    const index = this.productList.findIndex(
      product => product.id === updatedProduct.id,
    );

    runInAction(() => {
      this.productList[index] = {
        ...this.productList[index],
        ...updatedProduct,
      };
      this.isLoading = false;
    });

    Utils.onShowMessage(CHANGES_CONFIRMED, SUCCESS);
  };
}

const productsStore = new ProductsStore();
export default productsStore;
