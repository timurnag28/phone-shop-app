import {makeAutoObservable, runInAction} from 'mobx';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {CUSTOMER, EMPTY_FIELD, USER_DATA_STORAGE_KEY} from '../share/consts.ts';
import {Api} from '../services/Api.ts';
import {AuthData, BaseResponse} from '../inerfaces/interfaces.ts';

class AuthStore {
  constructor() {
    makeAutoObservable(this);
  }

  token: string = '';
  userRole: string | undefined = CUSTOMER;
  userName: string = '';
  password: string = '';
  errorMessage: string = '';
  isButtonLoading: boolean = false;
  isAuthLoading: boolean = false;

  onSetUserName = (userName: string) => {
    this.userName = userName;
  };

  onSetPassword = (password: string) => {
    this.password = password;
  };

  onLogin = () => {
    runInAction(() => {
      this.isButtonLoading = true;
      this.errorMessage = '';
    });

    const isAuthFieldEmpty = this.userName === '' || this.password === '';
    if (isAuthFieldEmpty) {
      return this.setErrorMessage(EMPTY_FIELD);
    }
    new Api()
      .onLogin(this.userName, this.password)
      .then(response => {
        void this.setTokenAndRole(response);
      })
      .catch(errorMessage => {
        this.setErrorMessage(errorMessage.message);
      });
  };

  setTokenAndRole = async (response: BaseResponse<AuthData>) => {
    let token = response.data.token!;
    let role = response.data.role!;
    await AsyncStorage.setItem(
      USER_DATA_STORAGE_KEY,
      JSON.stringify({token: token, userRole: role}),
    );
    runInAction(() => {
      this.token = token;
      this.userRole = role;
    });
    this.onClearAuthFields();
  };

  onClearAuthFields = () => {
    runInAction(() => {
      this.isButtonLoading = false;
      this.userName = '';
      this.password = '';
    });
  };

  setErrorMessage = (errorMessage: string) => {
    runInAction(() => {
      this.errorMessage = errorMessage;
      this.isButtonLoading = false;
    });
  };

  onGetAuthToken = async () => {
    this.isAuthLoading = true;
    const savedUserData = await AsyncStorage.getItem(USER_DATA_STORAGE_KEY);

    runInAction(() => {
      if (savedUserData) {
        const result = JSON.parse(savedUserData);
        this.token = result.token;
        this.userRole = result.userRole;
      }
      this.isAuthLoading = false;
    });
  };

  logout = async () => {
    await AsyncStorage.removeItem(USER_DATA_STORAGE_KEY);
    runInAction(() => {
      this.token = '';
    });
  };
}

const authStore = new AuthStore();
export default authStore;
