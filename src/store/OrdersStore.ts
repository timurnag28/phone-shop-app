import {makeAutoObservable, runInAction} from 'mobx';
import {
  BaseResponse,
  Field,
  Order,
  ProductItem,
} from '../inerfaces/interfaces.ts';
import {Api} from '../services/Api.ts';
import {orderFields} from '../share/FormFields.ts';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {ParamListBase} from '@react-navigation/native';
import Utils from '../share/Utils.ts';
import {DANGER, SUCCESS, SUCCESSFUL_ORDER} from '../share/consts.ts';

class OrdersStore {
  constructor() {
    makeAutoObservable(this);
  }

  ordersList: Order[] = [];
  orderFields: Field[] = orderFields;
  cartProductList: ProductItem[] = [];
  amountOfProductsInCart: number = 0;
  totalCartPrice: number = 0;
  isLoading: boolean = false;

  onGetOrders = () => {
    if (this.ordersList.length === 0) {
      this.isLoading = true;
      new Api()
        .onGetOrders()
        .then((response: BaseResponse<Order[]>) => {
          runInAction(() => {
            this.ordersList = response.data;
            this.isLoading = false;
          });
        })
        .catch((errorMessage: string) => {
          console.log(errorMessage);
        });
    }
  };

  addToCart = (productItem: ProductItem) => {
    const productIndex = this.cartProductList.findIndex(
      item => item.id === productItem.id,
    );
    runInAction(() => {
      if (productIndex !== -1) {
        this.cartProductList[productIndex].amount! += 1;
      } else {
        this.cartProductList.push({...productItem, amount: 1});
      }

      this.totalCartPrice += parseFloat(productItem.price);
      this.amountOfProductsInCart += 1;
    });
  };

  removeFromCart(productItem: ProductItem) {
    const productIndex = this.cartProductList.findIndex(
      item => item.id === productItem.id,
    );
    runInAction(() => {
      if (productIndex !== -1) {
        if (this.cartProductList[productIndex].amount! > 1) {
          this.cartProductList[productIndex].amount! -= 1;
        } else {
          this.cartProductList.splice(productIndex, 1);
        }

        this.totalCartPrice -= parseFloat(productItem.price);
        this.amountOfProductsInCart -= 1;
      }
    });
  }

  onConfirmOrder = (navigation: NativeStackNavigationProp<ParamListBase>) => {
    let errors = Utils.onCheckFields(this.orderFields);
    if (errors) {
      return Utils.onShowMessage(errors, DANGER);
    }
    this.isLoading = true;
    let newOrder = this.autoGenerateNewOrder();
    new Api()
      .onCreateOrder(newOrder)
      .then(() => this.autoConfirmSuccessOrder(navigation))
      .catch((errorMessage: string) => {
        this.isLoading = false;
        console.log(errorMessage);
      });
  };

  autoGenerateNewOrder = () => {
    let newOrder: Order = {
      orderId: 1,
      customerName: '',
      customerLastName: '',
      customerEmail: '',
      products: this.cartProductList,
      totalPrice: this.totalCartPrice,
    };

    this.orderFields.map(field => {
      newOrder[field.key] = field.value;
    });

    return newOrder;
  };

  autoConfirmSuccessOrder = (
    navigation: NativeStackNavigationProp<ParamListBase>,
  ) => {
    runInAction(() => {
      this.onClearOrderFields();
      this.onClearCart();
      this.isLoading = false;
    });

    Utils.onShowMessage(SUCCESSFUL_ORDER, SUCCESS);
    if (navigation.canGoBack()) {
      navigation.popToTop();
    }
  };

  setOrderField = (index: number, value: string) => {
    this.orderFields[index].value = value;
  };

  onClearOrderFields = () => {
    runInAction(() => {
      this.orderFields.map(field => {
        field.value = '';
      });
    });
  };

  onClearCart = () => {
    runInAction(() => {
      this.cartProductList = [];
      this.totalCartPrice = 0;
      this.amountOfProductsInCart = 0;
    });
  };

  onClearAdminData = () => {
    this.ordersList = [];
  };
}

const ordersStore = new OrdersStore();
export default ordersStore;
