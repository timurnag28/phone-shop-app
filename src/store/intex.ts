import productsStore from './ProductsStore.ts';
import ordersStore from './OrdersStore.ts';
import authStore from './AuthStore.ts';

const store = {
  productsStore,
  ordersStore,
  authStore,
};
export default store;
