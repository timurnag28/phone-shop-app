import AsyncStorage from '@react-native-async-storage/async-storage';
import {ordersData} from '../share/Mocks';
import {Order} from '../inerfaces/interfaces.ts';
import OrderService from '../services/OrdersService.ts';
import {ORDERS_STORAGE_KEY} from '../share/consts.ts';

// yarn jest OrderService.test

jest.mock('@react-native-async-storage/async-storage', () => ({
  setItem: jest.fn(),
  getItem: jest.fn(),
}));

describe('Test OrderService', () => {
  let orderService: OrderService;

  beforeEach(() => {
    orderService = new OrderService();
    jest.clearAllMocks();
  });

  describe('Test getOrders', () => {
    it('should return orders if they exist', async () => {
      const storedOrders = JSON.stringify(ordersData);
      (AsyncStorage.getItem as jest.Mock).mockResolvedValueOnce(storedOrders);

      const result = await orderService.getOrders();
      expect(AsyncStorage.getItem).toHaveBeenCalledWith(ORDERS_STORAGE_KEY);
      expect(result).toEqual({status: 200, data: ordersData});
    });

    it('should get an empty array and return it if there are no orders', async () => {
      (AsyncStorage.getItem as jest.Mock).mockResolvedValueOnce(null);

      const result = await orderService.getOrders();

      expect(AsyncStorage.getItem).toHaveBeenCalledWith(ORDERS_STORAGE_KEY);
      expect(AsyncStorage.setItem).toHaveBeenCalledWith(
        ORDERS_STORAGE_KEY,
        JSON.stringify(ordersData),
      );
      expect(result).toEqual({status: 200, data: []});
    });
  });

  describe('Test createOrder', () => {
    it('should create the first order with id 1 if there are no orders', async () => {
      const newOrder: Order = {
        orderId: 1,
        customerName: 'T',
        customerLastName: 'N',
        customerEmail: 'tim@gmail.com',
        products: [
          {
            id: 1,
            SKU: '548548',
            name: 'Samsung 10 Pro',
            description: 'The best phone 2',
            price: '2000',
            image: 'https://samsung.png',
            amount: 1,
          },
        ],
        totalPrice: 2000,
      };

      (AsyncStorage.getItem as jest.Mock).mockResolvedValueOnce(null);

      const result = await orderService.createOrder(newOrder);

      const expectedOrders = [
        {
          orderId: 1,
          customerName: newOrder.customerName,
          customerLastName: newOrder.customerLastName,
          customerEmail: newOrder.customerEmail,
          products: [...newOrder.products],
          totalPrice: newOrder.totalPrice,
        },
      ];

      expect(AsyncStorage.getItem).toHaveBeenCalledWith(ORDERS_STORAGE_KEY);
      expect(AsyncStorage.setItem).toHaveBeenCalledWith(
        ORDERS_STORAGE_KEY,
        JSON.stringify(expectedOrders),
      );
      expect(result).toEqual({status: 200, data: expectedOrders});
    });

    it('should create a new order which will have an ID 1 higher than the previous one', async () => {
      const previousOrder: Order = {
        orderId: 1,
        customerName: 'T',
        customerLastName: 'N',
        customerEmail: 'timurnag28@gmail.com',
        products: [
          {
            id: 1,
            SKU: '1234567',
            name: 'Iphone 20 Pro',
            description: 'The best phone',
            price: '999',
            image: 'https://iphone.png',
            amount: 1,
          },
        ],
        totalPrice: 100,
      };
      const previousOrders = [previousOrder];
      const previousStoredOrders = JSON.stringify(previousOrders);

      (AsyncStorage.getItem as jest.Mock).mockResolvedValueOnce(
        previousStoredOrders,
      );

      const newNextOrder: Order = {
        orderId: 0,
        customerName: 'T2',
        customerLastName: 'N2',
        customerEmail: 'timurnag28@gmail.com2',
        products: [
          {
            id: 1,
            SKU: '1234567',
            name: 'Iphone 20 Pro',
            description: 'The best phone',
            price: '999',
            image: 'https://iphone.png',
            amount: 1,
          },
        ],
        totalPrice: 100,
      };

      const expectedNewOrder = {
        orderId: previousOrder.orderId + 1,
        customerName: newNextOrder.customerName,
        customerLastName: newNextOrder.customerLastName,
        customerEmail: newNextOrder.customerEmail,
        products: [...newNextOrder.products],
        totalPrice: newNextOrder.totalPrice,
      };

      const updatedOrders = [...previousOrders, expectedNewOrder];

      const result = await orderService.createOrder(newNextOrder);

      expect(AsyncStorage.getItem).toHaveBeenCalledWith(ORDERS_STORAGE_KEY);
      expect(AsyncStorage.setItem).toHaveBeenCalledWith(
        ORDERS_STORAGE_KEY,
        JSON.stringify(updatedOrders),
      );
      expect(result).toEqual({status: 200, data: updatedOrders});
    });
  });
});
