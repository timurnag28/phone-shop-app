import {
  DESCRIPTION_FIELD_KEY,
  EMAIL,
  LAST_NAME,
  NAME_FIELD_KEY,
  PRICE_FIELD_KEY,
  SKU_FIELD_KEY,
  USER_NAME,
} from './consts.ts';
import {Field} from '../inerfaces/interfaces.ts';

export const orderFields: Field[] = [
  {
    key: USER_NAME,
    placeHolder: 'First Name',
    value: '',
  },

  {
    key: LAST_NAME,
    placeHolder: 'Last Name',
    value: '',
  },
  {
    key: EMAIL,
    placeHolder: 'Email',
    value: '',
  },
];

export const editableProductFields: Field[] = [
  {
    key: SKU_FIELD_KEY,
    placeHolder: 'SKU',
    value: '',
  },
  {
    key: NAME_FIELD_KEY,
    placeHolder: 'Product name',
    value: '',
  },

  {
    key: DESCRIPTION_FIELD_KEY,
    placeHolder: 'Description',
    value: '',
  },
  {
    key: PRICE_FIELD_KEY,
    placeHolder: 'Price',
    value: '',
  },
];
