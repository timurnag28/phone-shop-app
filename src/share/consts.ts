import {Dimensions} from 'react-native';

export const WINDOW_WIDTH = Dimensions.get('window').width;
export const WINDOW_HEIGHT = Dimensions.get('window').height;
export const imageSize = WINDOW_HEIGHT / 5;
// Font sizes
export const size12 = WINDOW_WIDTH / 34;
export const size14 = WINDOW_WIDTH / 30;
export const size16 = WINDOW_WIDTH / 26;
export const size18 = WINDOW_WIDTH / 23;
export const size20 = WINDOW_WIDTH / 21;
export const size22 = WINDOW_WIDTH / 19.6;
export const size24 = WINDOW_WIDTH / 18;
export const size28 = WINDOW_WIDTH / 15;
export const size32 = WINDOW_WIDTH / 13.5;
export const size34 = WINDOW_WIDTH / 13;
export const size36 = WINDOW_WIDTH / 12;
export const size44 = WINDOW_WIDTH / 10;

//Async Storage Keys:
export const PRODUCTS_STORAGE_KEY = 'Products';
export const ORDERS_STORAGE_KEY = 'Orders';
export const USER_DATA_STORAGE_KEY = 'Token_and_user_role';

//User roles
export const CUSTOMER = 'Customer';
export const ADMIN = 'Admin';

//Error messages
export const EMPTY_FIELD = 'Please enter your Username and Password.';
export const INCORRECT_USER = 'Please enter valid Username and Password.';
export const NO_DATA = 'No data available.';
export const DATA_ERROR = 'Error receiving data.';

//Order Fields
export const USER_NAME = 'customerName';
export const LAST_NAME = 'customerLastName';
export const EMAIL = 'customerEmail';

//Product editable fields
export const SKU_FIELD_KEY = 'SKU';
export const NAME_FIELD_KEY = 'name';
export const DESCRIPTION_FIELD_KEY = 'description';
export const PRICE_FIELD_KEY = 'price';

//Fields error messages
export const INVALID_EMAIL = 'Email is invalid.';
export const WRONG_PRICE = 'Wrong price.';

//FlashMessages
export const CHANGES_CONFIRMED = 'Changes confirmed!';
export const SUCCESSFUL_ORDER = 'You have successfully confirmed your order!';

//FlashMessages types
export const SUCCESS = 'success';
export const DANGER = 'danger';
