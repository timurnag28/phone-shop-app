import {MessageType, showMessage} from 'react-native-flash-message';
import {Field} from '../inerfaces/interfaces.ts';
import {EMAIL, INVALID_EMAIL, PRICE_FIELD_KEY, WRONG_PRICE} from './consts.ts';

export default class Utils {
  static onShowMessage(message: string, messageType: MessageType) {
    showMessage({
      message: message,
      type: messageType,
    });
  }

  static onCheckFields = (fieldsList: Field[]) => {
    const validField = /[a-zA-Z0-9]/;
    const validPriceRegex = /^\d+$/;
    const validEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    for (let item of fieldsList) {
      let fieldValue = item.value;
      let fieldKey = item.key;
      let placeHolder = item.placeHolder;

      const isInvalidField = !validField.test(fieldValue);

      const isInvalidEmail =
        fieldKey === EMAIL && !validEmailRegex.test(fieldValue.toLowerCase());

      const isValidPrice =
        (fieldKey === PRICE_FIELD_KEY && !validPriceRegex.test(fieldValue)) ||
        fieldValue[0] === '0';

      if (fieldValue === '') {
        return `${placeHolder} field is empty.`;
      }
      if (isInvalidField) {
        return `${placeHolder} field is invalid.`;
      }
      if (isInvalidEmail) {
        return INVALID_EMAIL;
      }
      if (isValidPrice) {
        return WRONG_PRICE;
      }
    }
  };
}
