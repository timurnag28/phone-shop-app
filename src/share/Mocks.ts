import {Order, User} from '../inerfaces/interfaces.ts';
import {ADMIN, CUSTOMER} from './consts.ts';

export const users: User[] = [
  {
    username: 'Client',
    password: 'Client',
    role: CUSTOMER,
  },
  {
    username: 'Admin',
    password: 'Admin',
    role: ADMIN,
  },
];

export const productsData = [
  {
    id: 1,
    SKU: '13328145-2',
    name: 'SAMSUNG Galaxy S24',
    description:
      'SAMSUNG Galaxy S24+ Plus Cell Phone, 256GB AI Smartphone, Unlocked Android, 50MP Camera, Fastest Processor, Long Battery Life, US Version 2024, Amber Yellow',
    price: '762',
    image:
      'https://i.pinimg.com/236x/05/bf/4e/05bf4e7dbf14fbb70f350045a04fcf22.jpg',
  },
  {
    id: 2,
    SKU: '49703653-4',
    name: 'Google Pixel 6a',
    description:
      'Front,Rear.Aspect ratio : 20:9. Up to 72-hour battery life with Extreme Battery Saver. Bluetooth Version 5.2 with dual antennas for enhanced quality and connection.',
    price: '199',
    image:
      'https://i.pinimg.com/474x/90/a4/f4/90a4f477f0caaaf455328bb7b8abfa6d.jpg',
  },
  {
    id: 3,
    SKU: '2084694-1',
    name: 'Google Pixel 7 Pro',
    description:
      'Google Pixel 7 Pro is Google’s best-of-everything phone; powered by Google Tensor G2',
    price: '381',
    image:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ-pua7peign-34CsfX-nbx_mK95JcFUAQOQ&s',
  },
  {
    id: 4,
    SKU: '34143745-8',
    name: 'SAMSUNG Galaxy S23',
    description:
      'GALAXY AI IS HERE: Search like never before¹, get real-time interpretation on a call², format your notes into a clear summary³, and effortlessly edit your photos⁴ - all from your smartphone, all with AI.',
    price: '511',
    image:
      'https://i.pinimg.com/236x/02/d5/ec/02d5ecb93e87d51cce6b6d72ed458d6f.jpg',
  },
  {
    id: 5,
    SKU: '50402712-0',
    name: 'OnePlus 12',
    description:
      'Pure Performance: The OnePlus 12 is powered by the latest Snapdragon 8 Gen 3, with up to 16GB of RAM.',
    price: '668',
    image:
      'https://i.pinimg.com/236x/95/65/98/956598caf1cb6d3e976f9f5fa27770de.jpg',
  },
  {
    id: 6,
    SKU: '12302712-3',
    name: 'Nokia 6300 4G',
    description:
      'Count on Nokia 6300 4G for smooth performance and signature durability.',
    price: '54',
    image:
      'https://i.pinimg.com/236x/8d/a5/78/8da57800a51b618ab10a695005ccb382.jpg',
  },
  {
    id: 7,
    SKU: '77705612-4',
    name: 'iPhone 15 Pro',
    description:
      '6.1-inch Super Retina XDR display footnote ¹ featuring ProMotion, Always-On, and Dynamic Island.',
    price: '999',
    image:
      'https://i.pinimg.com/236x/da/00/55/da0055eee8e90bd0e8c24bb7b1693594.jpg',
  },
  {
    id: 8,
    SKU: '79875612-4',
    name: 'iPhone 13',
    description: '6.1-inch Super Retina XDR display.',
    price: '599',
    image:
      'https://i.pinimg.com/236x/f5/d9/57/f5d95705969a52d7aaefc219284daa99.jpg',
  },
  {
    id: 9,
    SKU: '68305612-4',
    name: 'OnePlus 10T',
    description:
      '16GB RAM and the Always Alive feature deliver uncompromising multitasking.',
    price: '215',
    image:
      'https://i.pinimg.com/564x/da/4a/0e/da4a0e866699d6c2de242fca04e832f7.jpg',
  },
  {
    id: 10,
    SKU: '93493433-0',
    name: 'Motorola Moto G',
    description:
      'Built-in stylus. The stylus is perfect for navigating, highlighting, editing, or any task that requires pinpoint precision.',
    price: '114',
    image:
      'https://i.pinimg.com/236x/75/38/95/753895d6d9dabb47f4fbb2b37ac9ea83.jpg',
  },
  {
    id: 11,
    SKU: '68833312-4',
    name: 'Xiaomi Poco',
    description: 'In-screen fingerprint sensorAI Face Unlock.',
    price: '195',
    image:
      'https://i.pinimg.com/236x/97/d7/a5/97d7a52f96180b3275e0ff7ed37434f2.jpg',
  },
  {
    id: 12,
    SKU: '83339923-0',
    name: 'SAMSUNG Galaxy Z',
    description:
      'GALAXY AI IS HERE: Search like never before¹, get real-time interpretation on a call², format your notes into a clear summary³, and effortlessly edit your photos⁴ - all from your smartphone, all with AI.',
    price: '1117',
    image:
      'https://i.pinimg.com/236x/c7/94/1e/c7941e36b0be49b0a052ff421599dff7.jpg',
  },
];

export const ordersData: Order[] = [];
