import {SharedValue} from 'react-native-reanimated';
import {ROUTES} from '../navigation/routes';
import {NativeStackNavigationProp} from '@react-navigation/native-stack/lib/typescript/src/types';

export interface User {
  username: string;
  password: string;
  role: string;
}

export interface ProductItem {
  [key: string]: any;
  id: number;
  SKU: string;
  name: string;
  description: string;
  price: string;
  image: string;
  amount?: number;
}

export interface Order {
  [key: string]: any;
  orderId: number;
  customerName: string;
  customerLastName: string;
  customerEmail: string;
  products: ProductItem[];
  totalPrice: number;
}

export interface BottomSheetProps {
  toggleSheet: (value: boolean) => void;
  offset: SharedValue<number>;
  height: SharedValue<number>;
}

export interface Field {
  key: string;
  placeHolder: string;
  value: string;
}

export interface HeaderProps {
  toggleSheet?: (value: boolean, isAnimation?: boolean) => void;
}

export type RootStackParamList = {
  [ROUTES.AUTHORIZATION_SCREEN]: undefined;
  [ROUTES.CUSTOMER_HOME_SCREEN]: undefined;
  [ROUTES.ADMIN_HOME_SCREEN]: undefined;
  [ROUTES.CART_SCREEN]: undefined;
  [ROUTES.CHECKOUT_SCREEN]: undefined;
  [ROUTES.EDIT_PRODUCT_SCREEN]: undefined;
  [ROUTES.ORDERS_SCREEN]: undefined;
  [ROUTES.ORDER_HISTORY_SCREEN]: {orderDetails: Order};
  [ROUTES.ORDER_DETAILS_SCREEN]: {orderDetails: Order};
};

export interface BaseResponse<T> {
  status: number;
  data: T;
}

export interface AuthData {
  token?: string;
  role?: string;
  errorMessage?: string;
}
