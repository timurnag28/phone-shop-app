import React from 'react';
import {FlatList, ListRenderItem, StyleSheet, View} from 'react-native';
import store from '../../store/intex.ts';
import {observer} from 'mobx-react-lite';
import {ProductItem} from '../../inerfaces/interfaces.ts';
import {WINDOW_HEIGHT} from '../../share/consts.ts';
import ListEmptyComponent from '../common/ListEmptyComponent.tsx';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {ParamListBase, useNavigation} from '@react-navigation/native';
import {ROUTES} from '../../navigation/routes';
import BottomOrderButton from './components/BottomOrderButton';
import ProductInCart from './components/ProductInCart';

const CartScreen = () => {
  const {cartProductList, totalCartPrice} = store.ordersStore;
  const navigation: NativeStackNavigationProp<ParamListBase> = useNavigation();

  const renderItem: ListRenderItem<ProductItem> = ({item}) => {
    return <ProductInCart item={item} />;
  };

  const renderListEmptyComponent = () => {
    return <ListEmptyComponent title={'Cart is empty.'} />;
  };

  const navigateToCheckoutScreen = () => {
    navigation.navigate(ROUTES.CHECKOUT_SCREEN);
  };

  return (
    <View style={style.container}>
      <FlatList
        style={style.listContainer}
        contentContainerStyle={style.contentContainerStyle}
        data={cartProductList}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        ListEmptyComponent={renderListEmptyComponent}
      />
      <BottomOrderButton
        title={`Proceed to checkout ( ${totalCartPrice} $ )`}
        action={navigateToCheckoutScreen}
      />
    </View>
  );
};

const style = StyleSheet.create({
  container: {flex: 1},
  listContainer: {
    height: WINDOW_HEIGHT,
    margin: 6,
    flex: 1,
  },
  contentContainerStyle: {
    paddingBottom: WINDOW_HEIGHT / 12,
  },
});

export default observer(CartScreen);
