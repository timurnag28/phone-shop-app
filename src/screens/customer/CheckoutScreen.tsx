import { KeyboardAvoidingView, ScrollView, StyleSheet, View } from 'react-native';
import React, { useEffect } from 'react';
import store from '../../store/intex.ts';
import { observer } from 'mobx-react-lite';
import ButtonOverKeyboard from './components/ButtonOverKeyboard.tsx';
import PersonalInformation from './components/PersonalInformation';
import OrderList from './components/OrderList';

const CheckoutScreen = () => {
  useEffect(() => {
    return () => {
      store.ordersStore.onClearOrderFields();
    };
  }, []);

  return (
    <View style={style.container}>
      <KeyboardAvoidingView behavior={'padding'} style={style.keyBoard}>
        <ScrollView>
          <PersonalInformation />
          <OrderList />
        </ScrollView>
      </KeyboardAvoidingView>
      <ButtonOverKeyboard />
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  keyBoard: {
    flex: 1,
  },
});

export default observer(CheckoutScreen);
