import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {observer} from 'mobx-react-lite';
import store from '../../../store/intex';
import {size16, WINDOW_HEIGHT} from '../../../share/consts';

const BottomOrderButton = ({
  title,
  action,
}: {
  title: string;
  action: () => void;
}) => {
  const {isLoading, totalCartPrice} = store.ordersStore;
  return totalCartPrice ? (
    <TouchableOpacity
      disabled={isLoading}
      onPress={action}
      style={style.totalPriceBlock}>
      {!isLoading ? (
        <Text style={style.buttonText}>{title}</Text>
      ) : (
        <ActivityIndicator size={size16} color="#fff" />
      )}
    </TouchableOpacity>
  ) : null;
};

const style = StyleSheet.create({
  totalPriceBlock: {
    height: WINDOW_HEIGHT / 12,
    backgroundColor: '#7163ed',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    borderWidth: 1,
    borderColor: 'grey',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },

  buttonText: {
    textAlign: 'center',
    fontSize: size16,
    color: '#fff',
  },
});

export default observer(BottomOrderButton);
