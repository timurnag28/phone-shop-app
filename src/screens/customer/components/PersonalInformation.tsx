import {observer} from 'mobx-react-lite';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import {size14, WINDOW_WIDTH} from '../../../share/consts';
import store from '../../../store/intex';

const PersonalInformation = () => {
  const {orderFields, setOrderField} = store.ordersStore;

  const handleInputChange = (index: number, value: string) => {
    setOrderField(index, value);
  };

  return (
    <View style={style.fieldContainer}>
      <View style={style.container}>
        <Text style={style.header}>Personal information:</Text>
        {orderFields.map((item, index) => (
          <View key={item.key}>
            <TextInput
              style={style.inputStyle}
              placeholder={item.placeHolder}
              value={item.value}
              onChangeText={text => handleInputChange(index, text)}
            />
          </View>
        ))}
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    paddingLeft: 22,
    paddingVertical: 32,
  },
  fieldContainer: {
    backgroundColor: '#fff',
    width: WINDOW_WIDTH * 0.9,
    marginHorizontal: 20,
    marginTop: 6,
    borderRadius: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  header: {color: '#000', fontSize: size14, fontWeight: 'bold'},
  inputStyle: {
    width: WINDOW_WIDTH / 2,
    height: 40,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
    marginTop: 22,
  },
});

export default observer(PersonalInformation);
