import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import store from '../../../store/intex.ts';
import ProductImage from '../../common/ProductImage.tsx';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import {size14, size16, size22} from '../../../share/consts.ts';
import React from 'react';
import {ProductItem} from '../../../inerfaces/interfaces.ts';

interface ProductProps {
  item: ProductItem;
  toggleSheet: (value: boolean) => void;
}

const Product = ({item, toggleSheet}: ProductProps) => {
  const onShowBottomSheet = () => {
    const {currentProduct, isBottomSheetActive, setCurrentProduct} =
      store.productsStore;
    const isCurrentBottomSheetActive =
      item.id === currentProduct?.id && isBottomSheetActive;
    {
      if (isCurrentBottomSheetActive) {
        toggleSheet(false);
      } else {
        setCurrentProduct(item);
        toggleSheet(true);
      }
    }
  };

  return (
    <TouchableOpacity onPress={onShowBottomSheet} style={style.itemContainer}>
      <ProductImage item={item} />
      <View style={style.infoBlock}>
        <View style={{paddingVertical: 16}}>
          <Text numberOfLines={3} style={style.titleText}>
            {item.name}
          </Text>
          <Text style={style.priceText}>{`${item.price} $`}</Text>
        </View>
        <TouchableOpacity
          hitSlop={{top: 30, bottom: 30}}
          style={style.cart}
          onPress={() => store.ordersStore.addToCart(item)}>
          <Text style={style.cartText}>Add to cart</Text>
          <IconFontAwesome
            style={style.iconCart}
            name={'shopping-basket'}
            size={size16}
            color="#fff"
          />
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  itemContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    justifyContent: 'center',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginVertical: 6,
    marginHorizontal: 6,
    flex: 1,
  },

  infoBlock: {
    marginVertical: size22,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    flex: 1,
  },
  cart: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#7163ed',
    borderRadius: 20,
    paddingVertical: 8,
  },
  iconCart: {paddingLeft: 6},
  cartText: {fontSize: size14, color: '#fff'},
  titleText: {
    fontWeight: 'bold',
    fontSize: size16,
    color: '#000',
    textAlign: 'center',
  },
  priceText: {
    paddingTop: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: size16,
    color: '#7464ef',
  },
});

export default Product;
