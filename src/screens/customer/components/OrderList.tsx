import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {WINDOW_HEIGHT, WINDOW_WIDTH} from '../../../share/consts';
import store from '../../../store/intex';
import OrderedProductList from './../../common/OrderedProductList';
import {observer} from 'mobx-react-lite';
const OrderList = () => {
  return (
    <View style={style.orderInfoContainer}>
      <TotalPriceTitle />
      <OrderedProductList
        data={store.ordersStore.cartProductList}
        scrollEnabled={false}
      />
    </View>
  );
};

const TotalPriceTitle = observer(() => {
  return (
    <Text
      style={
        style.titleTotalPrice
      }>{`Total price: ${store.ordersStore.totalCartPrice} $`}</Text>
  );
});

const style = StyleSheet.create({
  keyBoard: {
    flex: 1,
    paddingTop: 22,
  },
  titleTotalPrice: {
    paddingTop: 12,
    paddingLeft: 22,
    color: '#000',
    fontWeight: 'bold',
  },

  orderInfoContainer: {
    backgroundColor: '#fff',
    width: WINDOW_WIDTH * 0.9,
    marginHorizontal: 20,
    marginTop: 22,
    marginBottom: WINDOW_HEIGHT / 5,
    borderRadius: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default OrderList;
