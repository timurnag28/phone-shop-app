import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import React from 'react';
import {
  imageSize,
  size12,
  size14,
  size16,
  size22,
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../../../share/consts';
import store from '../../../store/intex';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import ProductImage from '../../common/ProductImage';
import {observer} from 'mobx-react-lite';
import {ProductItem} from '../../../inerfaces/interfaces.ts';
interface ProductInCartProps {
  item: ProductItem;
}
const ProductInCart = ({item}: ProductInCartProps) => {
  return (
    <View style={style.itemContainer}>
      <ProductImage item={item} />
      <View>
        <View style={style.infoBlock}>
          <Text numberOfLines={3} style={style.titleText}>
            {item.name}
          </Text>
          <Text style={style.priceText}>{`${item.price} $`}</Text>
        </View>
        <View style={style.productNumber}>
          <TouchableOpacity
            hitSlop={{left: size12, right: size12, top: size12, bottom: size12}}
            onPress={() => store.ordersStore.removeFromCart(item)}>
            <IconFontAwesome name={'minus'} size={size16} color="#fff" />
          </TouchableOpacity>
          <Text style={style.productNumberText}>{item.amount}</Text>
          <TouchableOpacity
            hitSlop={{left: size12, right: size12, top: size12, bottom: size12}}
            onPress={() => store.ordersStore.addToCart(item)}>
            <IconFontAwesome name={'plus'} size={size16} color="#fff" />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {flex: 1},
  listContainer: {
    height: WINDOW_HEIGHT,
    margin: 6,
    flex: 1,
  },
  contentContainerStyle: {
    paddingBottom: WINDOW_HEIGHT / 12,
  },
  itemContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    flexDirection: 'row',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
  },
  image: {
    height: imageSize,
    width: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
  infoBlock: {
    marginVertical: size22,
    flex: 1,
    width: WINDOW_WIDTH / 2.5,
  },
  productNumber: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#7163ed',
    borderRadius: 20,
    paddingVertical: 8,
    paddingHorizontal: 4,
    width: WINDOW_WIDTH / 4,
    marginBottom: 22,
  },
  productNumberText: {fontSize: size16, color: '#fff', paddingHorizontal: 10},
  titleText: {fontWeight: 'bold', fontSize: size16, color: '#000'},
  priceText: {
    fontWeight: 'bold',
    fontSize: size14,
    color: '#7464ef',
    paddingTop: 2,
  },
});

export default observer(ProductInCart);
