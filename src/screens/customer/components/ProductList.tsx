import {observer} from 'mobx-react-lite';
import React from 'react';
import {ProductItem} from '../../../inerfaces/interfaces';
import store from '../../../store/intex';
import {FlatList, ListRenderItem, StyleSheet} from 'react-native';
import {WINDOW_HEIGHT} from '../../../share/consts';
import Product from './Product';
interface ProductListProps {
  toggleSheet: (value: boolean, isAnimation?: boolean) => void;
}
const ProductList = ({toggleSheet}: ProductListProps) => {
  const {filteredProductList, isBottomSheetActive} = store.productsStore;

  const renderItem: ListRenderItem<ProductItem> = ({item}) => {
    return <Product item={item} toggleSheet={toggleSheet} />;
  };

  return (
    <FlatList
      numColumns={2}
      style={style.listContainer}
      contentContainerStyle={
        isBottomSheetActive
          ? style.contentContainerWithSheetStyle
          : style.contentContainerStyle
      }
      data={filteredProductList}
      renderItem={renderItem}
      keyExtractor={item => item.id.toString()}
      columnWrapperStyle={style.columnWrapperStyle}
    />
  );
};

const style = StyleSheet.create({
  listContainer: {
    height: WINDOW_HEIGHT,
    margin: 6,
    flex: 1,
  },
  contentContainerWithSheetStyle: {
    paddingBottom: WINDOW_HEIGHT / 4,
  },
  contentContainerStyle: {paddingBottom: 10},
  columnWrapperStyle: {
    flex: 1,
    justifyContent: 'space-around',
  },
});

export default observer(ProductList);
