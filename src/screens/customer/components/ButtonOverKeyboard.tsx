import React from 'react';
import Animated, {
  useAnimatedKeyboard,
  useAnimatedStyle,
} from 'react-native-reanimated';

import store from '../../../store/intex.ts';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {ParamListBase, useNavigation} from '@react-navigation/native';
import BottomOrderButton from './BottomOrderButton.tsx';

const ButtonOverKeyboard = () => {
  const navigation: NativeStackNavigationProp<ParamListBase> = useNavigation();
  const keyboard = useAnimatedKeyboard();
  const translateStyle = useAnimatedStyle(() => {
    return {
      transform: [{translateY: -keyboard.height.value}],
    };
  });

  return (
    <Animated.View style={translateStyle}>
      <BottomOrderButton
        title={'Confirm order'}
        action={() => {
          store.ordersStore.onConfirmOrder(navigation);
        }}
      />
    </Animated.View>
  );
};

export default ButtonOverKeyboard;
