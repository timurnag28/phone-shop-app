import {
  BackHandler,
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {WINDOW_HEIGHT} from '../../share/consts.ts';
import Header from '../common/header/Header.tsx';
import {useSharedValue, withSpring, withTiming} from 'react-native-reanimated';
import {observer} from 'mobx-react-lite';
import store from '../../store/intex.ts';
import React, {useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import Loader from '../common/Loader.tsx';
import ProductList from './components/ProductList';
import BottomSheet from './components/BottomSheet';

const CustomerHomeScreen = () => {
  const offset = useSharedValue(0);
  const height = useSharedValue(0);
  const {isLoading, setBottomSheetActive, onCancelSearch, onGetProducts} =
    store.productsStore;
  const keyBoardBehavior = Platform.OS === 'ios' ? 'padding' : 'height';

  const toggleSheet = (value: boolean, isAnimation: boolean = true) => {
    if (value) {
      offset.value = withSpring(0);
      height.value = withTiming(WINDOW_HEIGHT / 4);
      setBottomSheetActive(true);
    } else {
      height.value = isAnimation ? withTiming(0) : 0;
      setBottomSheetActive(false);
    }
  };

  const backAction = () => {
    toggleSheet(false);
    return true;
  };

  const onCloseBottomSheet = () => {
    toggleSheet(false);
  };

  useEffect(() => {
    onGetProducts();
  }, []);

  useFocusEffect(() => {
    onCancelSearch();
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => {
      backHandler.remove();
      toggleSheet(false);
    };
  });

  if (isLoading) {
    return <Loader />;
  }
  return (
    <KeyboardAvoidingView style={style.keyBoard} behavior={keyBoardBehavior}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={onCloseBottomSheet}
        style={style.mainContainer}>
        <Header toggleSheet={toggleSheet} />
        <ProductList toggleSheet={toggleSheet} />
        <BottomSheet
          toggleSheet={toggleSheet}
          offset={offset}
          height={height}
        />
      </TouchableOpacity>
    </KeyboardAvoidingView>
  );
};

const style = StyleSheet.create({
  keyBoard: {flex: 1},
  mainContainer: {
    backgroundColor: '#f4f5f7',
    flex: 1,
  },
});

export default observer(CustomerHomeScreen);
