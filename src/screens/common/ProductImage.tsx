import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {imageSize} from '../../share/consts';
import {observer} from 'mobx-react-lite';
import {ProductItem} from '../../inerfaces/interfaces.ts';
interface ProductImageProps {
  item: ProductItem;
}
const ProductImage = ({item}: ProductImageProps) => {
  return (
    <View style={style.container}>
      {item.image ? (
         <Image
          style={style.image}
          source={{
            uri: item.image,
          }}
          resizeMode={'contain'}
        />
      ) : (
        <View style={style.image}>
          <Text>No image</Text>
        </View>
      )}
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: imageSize,
    width: imageSize,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default observer(ProductImage);
