import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {size16} from '../../share/consts.ts';
import React from 'react';

const Loader = () => {
  return (
    <View style={style.container}>
      <ActivityIndicator size={size16} color="#7163ed" />
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Loader;
