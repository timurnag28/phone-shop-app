import {FlatList, ListRenderItem, StyleSheet} from 'react-native';
import React from 'react';
import {ProductItem} from '../../inerfaces/interfaces.ts';
import OrderedProduct from './OrderedProduct.tsx';
import {observer} from 'mobx-react-lite';

export interface IProps {
  data: ProductItem[];
  scrollEnabled?: boolean;
}
const OrderedProductList = ({data, scrollEnabled = true}: IProps) => {
  const renderItem: ListRenderItem<ProductItem> = ({item}) => {
    return <OrderedProduct item={item} />;
  };

  return (
    <FlatList
      scrollEnabled={scrollEnabled}
      style={style.listContainer}
      contentContainerStyle={style.contentListContainer}
      data={data}
      renderItem={renderItem}
      keyExtractor={item => item.id.toString()}
    />
  );
};

const style = StyleSheet.create({
  listContainer: {
    margin: 6,
    flex: 1,
  },
  contentListContainer: {
    paddingBottom: 12,
  },
});

export default observer(OrderedProductList);
