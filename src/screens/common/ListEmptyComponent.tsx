import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {size18, WINDOW_HEIGHT} from '../../share/consts.ts';

const ListEmptyComponent = ({title}: {title: string}) => (
  <View style={style.emptyComponent}>
    <Text style={style.textEmptyComponent}>{title}</Text>
  </View>
);

const style = StyleSheet.create({
  textEmptyComponent: {fontSize: size18, color: '#000'},
  emptyComponent: {
    justifyContent: 'center',
    alignItems: 'center',
    height: WINDOW_HEIGHT / 2,
  },
});

export default ListEmptyComponent;
