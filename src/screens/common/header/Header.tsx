import React from 'react';
import {observer} from 'mobx-react-lite';
import {HeaderProps} from '../../../inerfaces/interfaces.ts';
import {StyleSheet, View} from 'react-native';
import {WINDOW_HEIGHT, WINDOW_WIDTH} from '../../../share/consts.ts';
import LeftIcon from './components/LeftIcon.tsx';
import RightIcon from './components/RightIcon.tsx';
import SearchInput from './components/SearchInput.tsx';

const Header = ({toggleSheet}: HeaderProps) => {
  return (
    <View style={style.container}>
      <LeftIcon />
      <SearchInput toggleSheet={toggleSheet} />
      <RightIcon />
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    height: WINDOW_HEIGHT / 12,
    width: WINDOW_WIDTH,
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },
  searchInput: {
    width: WINDOW_WIDTH / 2,
    height: WINDOW_WIDTH / 9,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
  },
});

export default observer(Header);
