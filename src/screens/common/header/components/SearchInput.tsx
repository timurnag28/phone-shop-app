import {observer} from 'mobx-react-lite';
import {HeaderProps} from '../../../../inerfaces/interfaces.ts';
import store from '../../../../store/intex.ts';
import {CUSTOMER, WINDOW_WIDTH} from '../../../../share/consts.ts';
import {StyleSheet, TextInput, View} from 'react-native';
import React, {useEffect} from 'react';

const SearchInput = observer(({toggleSheet}: HeaderProps) => {
  const {searchText, setSearchText, onSearchProduct} = store.productsStore;
  const {userRole} = store.authStore;

  const onFocusSearchInput = () => {
    toggleSheet ? toggleSheet(false, false) : null;
  };
  const isCustomer = userRole === CUSTOMER;

  useEffect(() => {
    const timeOut = setTimeout(() => onSearchProduct(searchText), 500);
    return () => clearTimeout(timeOut);
  }, [searchText]);

  return isCustomer ? (
    <TextInput
      onTouchStart={onFocusSearchInput}
      style={style.searchInput}
      placeholder="Search"
      value={searchText}
      onChangeText={setSearchText}
    />
  ) : (
    <View style={style.gap} />
  );
});

const style = StyleSheet.create({
  searchInput: {
    width: WINDOW_WIDTH / 2,
    height: WINDOW_WIDTH / 9,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
  },
  gap: {width: WINDOW_WIDTH / 2},
});

export default SearchInput;
