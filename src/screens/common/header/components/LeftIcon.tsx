import {observer} from 'mobx-react-lite';
import store from '../../../../store/intex.ts';
import {StyleSheet, TouchableOpacity} from 'react-native';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import {size24} from '../../../../share/consts.ts';
import React from 'react';

const LeftIcon = observer(() => {
  const {onClearAdminData, onClearCart} = store.ordersStore;
  const {logout} = store.authStore;

  const onExit = () => {
    void logout();
    onClearAdminData();
    onClearCart();
  };
  return (
    <TouchableOpacity onPress={onExit} style={style.ordersIcon}>
      <IconIonicons name={'exit'} size={size24} color="#000" />
    </TouchableOpacity>
  );
});

const style = StyleSheet.create({
  ordersIcon: {padding: 12},
});

export default LeftIcon;
