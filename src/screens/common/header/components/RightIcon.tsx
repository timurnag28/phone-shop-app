import {observer} from 'mobx-react-lite';
import store from '../../../../store/intex.ts';
import {NavigationProp, useNavigation} from '@react-navigation/native';
import {RootStackParamList} from '../../../../inerfaces/interfaces.ts';
import {ROUTES} from '../../../../navigation/routes.ts';
import {
  ADMIN,
  size12,
  size24,
  WINDOW_HEIGHT,
  WINDOW_WIDTH,
} from '../../../../share/consts.ts';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import React from 'react';

const RightIcon = observer(() => {
  const {userRole} = store.authStore;
  const {amountOfProductsInCart} = store.ordersStore;

  const navigation: NavigationProp<RootStackParamList> = useNavigation();

  const onNavigateToCart = () => {
    navigation.navigate(ROUTES.CART_SCREEN);
  };
  const onNavigateToOrders = () => {
    navigation.navigate(ROUTES.ORDERS_SCREEN);
  };

  const isAdmin = userRole === ADMIN;
  const priceIsNull = amountOfProductsInCart === 0;

  return isAdmin ? (
    <TouchableOpacity onPress={onNavigateToOrders} style={style.ordersIcon}>
      <IconIonicons name={'list-outline'} size={size24} color="#000" />
    </TouchableOpacity>
  ) : (
    <TouchableOpacity
      onPress={onNavigateToCart}
      style={style.cartIconContainer}>
      <IconFontAwesome
        style={style.cartIcon}
        name={'shopping-cart'}
        size={size24}
        color="#000"
      />
      {!priceIsNull && (
        <View style={style.amountCircle}>
          <Text style={style.amountText}>{amountOfProductsInCart}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
});

const style = StyleSheet.create({
  container: {
    height: WINDOW_HEIGHT / 12,
    width: WINDOW_WIDTH,
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
  },

  ordersIcon: {padding: 12},
  cartIconContainer: {padding: 12},
  cartIcon: {paddingLeft: 6},
  amountCircle: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: '#7163ed',
    height: size24,
    width: size24,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  amountText: {color: '#fff', fontSize: size12},
});

export default RightIcon;
