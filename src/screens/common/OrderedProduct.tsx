import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {size14, size16, size22, WINDOW_WIDTH} from '../../share/consts';
import ProductImage from './ProductImage';
import {ProductItem} from '../../inerfaces/interfaces.ts';
interface ProductElementProps {
  item: ProductItem;
}
const OrderedProduct = ({item}: ProductElementProps) => {
  return (
    <View style={style.itemContainer}>
      <ProductImage item={item} />
      <View style={style.infoBlock}>
        <>
          <Text numberOfLines={3} style={style.titleText}>
            {item.name}
          </Text>
          <Text style={style.priceText}>{`${item.price} $`}</Text>
        </>
        <Text style={style.productNumberText}>{`Qty: ${item.amount}`}</Text>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  itemContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    flexDirection: 'row',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
  },

  infoBlock: {
    marginLeft: 12,
    marginVertical: size22,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    width: WINDOW_WIDTH / 3.5,
  },

  productNumberText: {fontSize: size16, color: '#000'},
  titleText: {fontWeight: 'bold', fontSize: size16, color: '#000'},
  priceText: {
    fontWeight: 'bold',
    fontSize: size14,
    color: '#7464ef',
    paddingTop: 2,
  },
});

export default OrderedProduct;
