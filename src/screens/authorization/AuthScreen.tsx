import {KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';
import React from 'react';
import {observer} from 'mobx-react-lite';
import {size16, WINDOW_WIDTH} from '../../share/consts.ts';
import TooltipBlock from './components/TooltipBlock.tsx';
import AuthInputs from './components/AuthInputs.tsx';
import ErrorMessage from './components/ErrorMessage.tsx';
import LoginButton from './components/LoginButton.tsx';

const AuthScreen = () => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={style.container}>
      <AuthInputs />
      <ErrorMessage />
      <LoginButton />
      <TooltipBlock />
    </KeyboardAvoidingView>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  authInput: {
    width: WINDOW_WIDTH / 2,
    height: 40,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
    marginTop: 12,
  },
  loginButton: {
    width: WINDOW_WIDTH / 2,
    height: 40,
    backgroundColor: '#7163ed',
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 12,
  },
  loginText: {color: '#fff', fontSize: size16},
  toolTipContainer: {
    width: WINDOW_WIDTH / 2,
    marginTop: 12,
    alignItems: 'center',
  },
});

export default observer(AuthScreen);
