import {observer} from 'mobx-react-lite';
import store from '../../../store/intex.ts';
import {StyleSheet, Text, View} from 'react-native';
import {size14, WINDOW_WIDTH} from '../../../share/consts.ts';
import React from 'react';

const ErrorMessage = () => {
  const {errorMessage} = store.authStore;
  return (
    <View style={style.container}>
      <Text style={style.errorMessage}>{errorMessage}</Text>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    width: WINDOW_WIDTH / 2,
    justifyContent: 'center',
  },
  errorMessage: {color: 'red', fontSize: size14, textAlign: 'center'},
});

export default observer(ErrorMessage);
