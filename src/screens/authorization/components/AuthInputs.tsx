import {StyleSheet, TextInput} from 'react-native';
import React from 'react';
import {View} from 'react-native';
import store from '../../../store/intex.ts';
import {WINDOW_WIDTH} from '../../../share/consts.ts';
import {observer} from 'mobx-react-lite';

const AuthInputs = () => {
  const {userName, password, onSetPassword, onSetUserName} = store.authStore;
  return (
    <>
      <TextInput
        style={style.authInput}
        placeholder="Username"
        value={userName}
        onChangeText={onSetUserName}
      />
      <TextInput
        style={style.authInput}
        placeholder="Password"
        secureTextEntry={true}
        value={password}
        onChangeText={onSetPassword}
      />
    </>
  );
};
const style = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  authInput: {
    width: WINDOW_WIDTH / 2,
    height: 40,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
    marginTop: 12,
  },
});
export default observer(AuthInputs);
