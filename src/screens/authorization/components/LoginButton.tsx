import {observer} from 'mobx-react-lite';
import store from '../../../store/intex.ts';
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import {size16, WINDOW_WIDTH} from '../../../share/consts.ts';
import React from 'react';

const LoginButton = () => {
  const {isButtonLoading, onLogin} = store.authStore;
  return (
    <TouchableOpacity
      hitSlop={{top: 20, bottom: 20}}
      disabled={isButtonLoading}
      style={style.loginButton}
      onPress={onLogin}>
      {!isButtonLoading ? (
        <Text style={style.loginText}>Login</Text>
      ) : (
        <ActivityIndicator size={size16} color="#fff" />
      )}
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  authInput: {
    width: WINDOW_WIDTH / 2,
    height: 40,
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    paddingLeft: 10,
    marginTop: 12,
  },
  loginButton: {
    width: WINDOW_WIDTH / 2,
    height: 40,
    backgroundColor: '#7163ed',
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 12,
  },
  loginText: {color: '#fff', fontSize: size16},
  toolTipContainer: {
    width: WINDOW_WIDTH / 2,
    marginTop: 12,
    alignItems: 'center',
  },
});

export default observer(LoginButton);
