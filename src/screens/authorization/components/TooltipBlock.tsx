import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {size14, WINDOW_WIDTH} from '../../../share/consts.ts';

const TooltipBlock = () => {
  return (
    <View style={style.container}>
      <View style={style.toolTipContainer}>
        <TooltipText title={'Client login:'} text={'Client'} />
        <TooltipText title={'Client password:'} text={'Client'} />
      </View>
      <View style={style.toolTipContainer}>
        <TooltipText title={'Admin login:'} text={'Admin'} />
        <TooltipText title={'Admin password:'} text={'Admin'} />
      </View>
    </View>
  );
};

const TooltipText = ({title, text}: {title: string; text: string}) => {
  return (
    <View style={style.tooltipContainer}>
      <Text style={style.leftText}>{title}</Text>
      <Text style={style.rightText}>{text}</Text>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  toolTipContainer: {
    width: WINDOW_WIDTH / 2,
    marginTop: 12,
    alignItems: 'center',
  },
  tooltipContainer: {
    flexDirection: 'row',
    width: WINDOW_WIDTH / 2.5,
    justifyContent: 'space-between',
  },
  leftText: {fontSize: size14},
  rightText: {fontSize: size14, paddingLeft: 20},
});

export default TooltipBlock;
