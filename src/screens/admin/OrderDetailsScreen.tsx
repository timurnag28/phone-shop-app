import React from 'react';
import {RootStackParamList} from '../../inerfaces/interfaces.ts';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import CustomOrderedProductList from '../common/OrderedProductList.tsx';
import {StyleSheet, View} from 'react-native';

import {ROUTES} from '../../navigation/routes.ts';
import OrderInfoBlock from './components/OrderInfoBlock.tsx';

type Props = NativeStackScreenProps<
  RootStackParamList,
  ROUTES.ORDER_DETAILS_SCREEN
>;
const OrderDetailsScreen = ({route}: Props) => {
  const {orderDetails} = route.params;
  return (
    <View style={style.container}>
      <OrderInfoBlock orderDetails={orderDetails} />
      <CustomOrderedProductList data={orderDetails.products} />
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
});

export default OrderDetailsScreen;
