import React, {useEffect} from 'react';
import {observer} from 'mobx-react-lite';
import store from '../../store/intex.ts';
import {Order, RootStackParamList} from '../../inerfaces/interfaces.ts';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import ListEmptyComponent from '../common/ListEmptyComponent.tsx';
import {FlatList, ListRenderItem, StyleSheet, View} from 'react-native';
import {WINDOW_HEIGHT} from '../../share/consts.ts';
import Loader from '../common/Loader.tsx';
import {ROUTES} from '../../navigation/routes';
import OrderedItem from './components/OrderedItem.tsx';

type Props = NativeStackScreenProps<RootStackParamList, ROUTES.ORDERS_SCREEN>;

const OrdersScreen = ({navigation}: Props) => {
  const {ordersList, isLoading, onGetOrders} = store.ordersStore;

  useEffect(() => {
    onGetOrders();
  }, []);

  if (isLoading) {
    return <Loader />;
  }

  const renderItem: ListRenderItem<Order> = ({item}: {item: Order}) => {
    return <OrderedItem item={item} navigation={navigation} />;
  };

  return (
    <View style={style.ordersContainer}>
      <FlatList
        style={style.listContainer}
        contentContainerStyle={style.listContentContainer}
        data={ordersList}
        renderItem={renderItem}
        keyExtractor={item => item.orderId.toString()}
        ListEmptyComponent={renderListEmptyComponent}
      />
    </View>
  );
};

const renderListEmptyComponent = () => {
  return <ListEmptyComponent title={'Orders history is empty.'} />;
};

const style = StyleSheet.create({
  ordersContainer: {flex: 1, alignItems: 'center'},
  listContainer: {
    height: WINDOW_HEIGHT,
    flex: 1,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },

  listContentContainer: {
    paddingBottom: 10,
  },
});

export default observer(OrdersScreen);
