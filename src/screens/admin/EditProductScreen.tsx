import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {observer} from 'mobx-react-lite';
import store from '../../store/intex.ts';
import React, {useEffect} from 'react';
import Loader from '../common/Loader.tsx';
import ProductsTextInputs from './components/ProductsTextInputs.tsx';

const EditProductScreen = () => {
  const {isLoading, fieldHasChangedByUser} = store.productsStore;

  useEffect(() => {
    return fieldHasChangedByUser(false);
  }, []);

  if (isLoading) {
    return <Loader />;
  }
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={style.keyBoard}>
      <ScrollView style={style.container}>
        <ProductsTextInputs />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const style = StyleSheet.create({
  keyBoard: {
    flex: 1,
  },
  container: {
    backgroundColor: '#fff',
    borderTopRightRadius: 12,
    borderTopLeftRadius: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    paddingHorizontal: 12,
    flex: 1,
  },
});

export default observer(EditProductScreen);
