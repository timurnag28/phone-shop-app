import {observer} from 'mobx-react-lite';
import store from '../../../store/intex.ts';
import {TouchableOpacity, View} from 'react-native';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import {size22, size32} from '../../../share/consts.ts';
import React from 'react';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {ParamListBase, useNavigation} from '@react-navigation/native';

const OrdersBackButton = () => {
  const navigation: NativeStackNavigationProp<ParamListBase> = useNavigation();
  return !store.productsStore.isLoading ? (
    <TouchableOpacity
      style={{paddingRight: 32}}
      onPress={() => navigation.goBack()}>
      <IconIonicons name={'arrow-back-sharp'} size={size22} color="#000" />
    </TouchableOpacity>
  ) : (
    <View style={{width: size32, paddingRight: 32}} />
  );
};

export default observer(OrdersBackButton);
