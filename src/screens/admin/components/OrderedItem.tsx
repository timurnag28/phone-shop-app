import {Order, RootStackParamList} from '../../../inerfaces/interfaces.ts';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ROUTES} from '../../../navigation/routes.ts';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import {size14, size16, size22, WINDOW_WIDTH} from '../../../share/consts.ts';
import React from 'react';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
type Props = NativeStackScreenProps<RootStackParamList, ROUTES.ORDERS_SCREEN>;
interface OrderProps {
  item: Order;
  navigation: Props['navigation'];
}

const OrderedItem = ({item, navigation}: OrderProps) => {
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate(ROUTES.ORDER_DETAILS_SCREEN, {
          orderDetails: item,
        });
      }}
      style={style.itemContainer}>
      <View style={style.infoBlock}>
        <Text style={style.titleText}>{`Order number: ${item.orderId}`}</Text>
        <Text
          style={
            style.titleText
          }>{`Buyer: ${item.customerName} ${item.customerLastName}`}</Text>
        <Text style={style.totalText}>
          Order total:
          <Text style={style.priceText}>{` ${item.totalPrice} $`}</Text>
        </Text>
      </View>
      <View style={style.orderBlock}>
        <IconFontAwesome name={'chevron-right'} size={size16} color="#000" />
      </View>
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  itemContainer: {
    width: WINDOW_WIDTH * 0.9,
    backgroundColor: '#fff',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
  },
  infoBlock: {
    marginLeft: 12,
    marginVertical: size22,
    justifyContent: 'space-between',
    backgroundColor: '#fff',
  },
  totalText: {color: '#000', fontSize: size14},
  titleText: {fontWeight: 'bold', fontSize: size16, color: '#000'},
  priceText: {
    fontWeight: 'bold',
    fontSize: size14,
    color: '#7464ef',
    paddingTop: 2,
  },
  orderBlock: {paddingRight: 12},
});

export default OrderedItem;
