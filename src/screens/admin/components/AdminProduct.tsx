import {StyleSheet, View} from 'react-native';
import ProductImage from '../../common/ProductImage.tsx';
import store from '../../../store/intex.ts';
import {ROUTES} from '../../../navigation/routes.ts';
import {size22, WINDOW_HEIGHT} from '../../../share/consts.ts';
import React from 'react';
import {NativeStackNavigationProp} from '@react-navigation/native-stack/lib/typescript/src/types';
import ProductInfo from './ProductInfo.tsx';
import {observer} from 'mobx-react-lite';
import {
  ProductItem,
  RootStackParamList,
} from '../../../inerfaces/interfaces.ts';
import EditButton from './EditButton.tsx';

type navigationProps = NativeStackNavigationProp<
  RootStackParamList,
  ROUTES.ADMIN_HOME_SCREEN
>;
interface AdminProductProps {
  item: ProductItem;
  navigation: navigationProps;
}

const AdminProduct = ({item, navigation}: AdminProductProps) => {
  const goToEditProduct = () => {
    store.productsStore.onFillEditableProductFields(item);
    navigation.navigate(ROUTES.EDIT_PRODUCT_SCREEN);
  };

  return (
    <View style={style.itemContainer}>
      <ProductImage item={item} />
      <View style={style.infoBlock}>
        <ProductInfo item={item} />
        <EditButton goToEditProduct={goToEditProduct} />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  itemContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    justifyContent: 'center',
    paddingHorizontal: 12,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    marginHorizontal: 12,
    flex: 1,
    height: WINDOW_HEIGHT / 2,
  },
  infoBlock: {
    marginLeft: 12,
    marginVertical: size22,
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    flex: 1,
  },
});

export default observer(AdminProduct);
