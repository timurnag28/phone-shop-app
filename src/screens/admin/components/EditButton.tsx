import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome5';
import {size14, size16} from '../../../share/consts.ts';
import React from 'react';

const EditButton = ({goToEditProduct}: {goToEditProduct: () => void}) => {
  return (
    <TouchableOpacity
      hitSlop={{top: 20, bottom: 20}}
      style={style.cart}
      onPress={goToEditProduct}>
      <Text style={style.cartText}>Edit product</Text>
      <IconFontAwesome
        style={style.iconCart}
        name={'edit'}
        size={size16}
        color="#fff"
      />
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  cart: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#7163ed',
    borderRadius: 20,
    paddingVertical: 8,
  },
  iconCart: {paddingLeft: 6},
  cartText: {fontSize: size14, color: '#fff'},
});

export default EditButton;
