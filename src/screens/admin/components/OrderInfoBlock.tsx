import {Order} from '../../../inerfaces/interfaces.ts';
import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {size14} from '../../../share/consts.ts';

const OrderInfoBlock = ({orderDetails}: {orderDetails: Order}) => {
  return (
    <View style={style.infoContainer}>
      <TextRow title={'Order number:'} text={orderDetails.orderId.toString()} />
      <TextRow
        title={'Name:'}
        text={`${orderDetails.customerName} ${orderDetails.customerLastName}`}
      />
      <TextRow title={'Email:'} text={`${orderDetails.customerEmail}`} />
      <TextRow title={'Order total:'} text={`${orderDetails.totalPrice} $`} />
    </View>
  );
};

const TextRow = ({title, text}: {title: string; text: string}) => {
  return (
    <View style={style.textRow}>
      <Text style={style.title}>{title}</Text>
      <Text style={style.text}>{text}</Text>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  infoContainer: {
    backgroundColor: '#fff',
    borderRadius: 16,
    marginHorizontal: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 12,
    alignItems: 'center',
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 20,
  },
  title: {
    color: '#000',
    fontSize: size14,
    fontWeight: 'bold',
    paddingVertical: 6,
    flex: 1,
  },
  text: {
    color: '#000',
    fontSize: size14,

    paddingVertical: 6,
    flex: 1,
  },
});

export default OrderInfoBlock;
