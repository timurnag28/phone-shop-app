import {TouchableOpacity} from 'react-native';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import {size28} from '../../../share/consts.ts';
import React from 'react';
import {observer} from 'mobx-react-lite';
import store from '../../../store/intex.ts';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {ParamListBase, useNavigation} from '@react-navigation/native';

const SaveFieldsButton = () => {
  const navigation: NativeStackNavigationProp<ParamListBase> = useNavigation();
  if (!store.productsStore.isSomeFieldHasChanged) {
    return null;
  }
  return (
    <TouchableOpacity
      onPress={() => store.productsStore.onConfirmUpdateProduct(navigation)}
      style={{}}>
      <IconIonicons name={'checkmark'} size={size28} color="#000" />
    </TouchableOpacity>
  );
};

export default observer(SaveFieldsButton);
