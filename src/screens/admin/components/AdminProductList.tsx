import {FlatList, ListRenderItem, StyleSheet} from 'react-native';
import React from 'react';
import {observer} from 'mobx-react-lite';
import {useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack/lib/typescript/src/types';
import store from '../../../store/intex.ts';
import {ROUTES} from '../../../navigation/routes.ts';
import {WINDOW_HEIGHT} from '../../../share/consts.ts';
import {
  ProductItem,
  RootStackParamList,
} from '../../../inerfaces/interfaces.ts';
import AdminProduct from './AdminProduct.tsx';

type navigationProps = NativeStackNavigationProp<
  RootStackParamList,
  ROUTES.ADMIN_HOME_SCREEN
>;
const AdminProductList = () => {
  const navigation: navigationProps = useNavigation();
  const {filteredProductList} = store.productsStore;

  const renderItem: ListRenderItem<ProductItem> = ({item}) => {
    return <AdminProduct item={item} navigation={navigation} />;
  };

  return (
    <FlatList
      style={style.listContainer}
      contentContainerStyle={style.contentContainerStyle}
      data={filteredProductList}
      renderItem={renderItem}
      keyExtractor={item => item.id.toString()}
    />
  );
};

const style = StyleSheet.create({
  listContainer: {
    height: WINDOW_HEIGHT,
    margin: 6,
    flex: 1,
  },

  contentContainerStyle: {paddingBottom: 10},
});

export default observer(AdminProductList);
