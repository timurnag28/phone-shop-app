import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {size14} from '../../../share/consts.ts';
import {ProductItem} from '../../../inerfaces/interfaces.ts';

const ProductInfo = ({item}: {item: ProductItem}) => {
  return (
    <View style={{flex: 1}}>
      <TextRow title={'SKU:'} text={`${item.SKU}`} />
      <TextRow title={'Name:'} text={`${item.name}`} />
      <TextRow title={'Description:'} text={`${item.description}`} />
      <TextRow title={'Price:'} text={`${item.price} $`} />
    </View>
  );
};
const TextRow = ({title, text}: {title: string; text: string}) => {
  return (
    <View style={style.textRowContainer}>
      <View style={{flex: 1, backgroundColor: 'transparent'}}>
        <Text style={style.titleText}>{title}</Text>
      </View>

      <View style={{flex: 2, backgroundColor: 'transparent'}}>
        <Text numberOfLines={2} style={style.text}>
          {text}
        </Text>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  textRowContainer: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    justifyContent: 'space-between',
    flex: 1,
  },

  titleText: {
    fontSize: size14,
    color: '#000',
    fontWeight: 'bold',
    flex: 1,
  },
  text: {
    fontSize: size14,
    color: '#000',
    paddingLeft: 12,
    flex: 1,
  },
});

export default ProductInfo;
