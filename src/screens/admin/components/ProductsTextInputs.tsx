import {StyleSheet, Text, TextInput, View} from 'react-native';
import React from 'react';
import store from '../../../store/intex.ts';
import {observer} from 'mobx-react-lite';
import {
  DESCRIPTION_FIELD_KEY,
  PRICE_FIELD_KEY,
  size14,
  WINDOW_HEIGHT,
} from '../../../share/consts.ts';

const ProductsTextInputs = () => {
  const {setEditableProductField, editableProductFields} = store.productsStore;
  const handleInputChange = (index: number, value: string) => {
    setEditableProductField(index, value);
  };

  return (
    <View style={style.fieldsContainer}>
      {editableProductFields.map((item, index) => {
        const isPriceField = item.key === PRICE_FIELD_KEY;
        const isDescriptionField = item.key === DESCRIPTION_FIELD_KEY;
        return (
          <View key={item.key} style={style.titleContainer}>
            <Text style={style.title}>{`${item.placeHolder}:`}</Text>
            <TextInput
              multiline={isDescriptionField}
              keyboardType={isPriceField ? 'number-pad' : 'default'}
              maxLength={isPriceField ? 6 : 200}
              style={
                !isDescriptionField
                  ? style.inputDescriptionFieldStyle
                  : style.inputStyle
              }
              placeholder={item.placeHolder}
              value={item.value}
              onChangeText={text => handleInputChange(index, text)}
            />
          </View>
        );
      })}
    </View>
  );
};

const style = StyleSheet.create({
  titleContainer: {
    paddingVertical: 12,
    paddingLeft: 6,
    flex: 1,
  },
  title: {
    fontSize: size14,
    color: '#000',
    fontWeight: 'bold',
  },
  inputStyle: {
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    marginTop: 6,
    paddingLeft: 12,
  },
  inputDescriptionFieldStyle: {
    backgroundColor: '#f4f5f7',
    borderRadius: 16,
    marginTop: 6,
    height: 40,
    paddingLeft: 12,
  },
  fieldsContainer: {marginBottom: WINDOW_HEIGHT / 5},
});

export default observer(ProductsTextInputs);
