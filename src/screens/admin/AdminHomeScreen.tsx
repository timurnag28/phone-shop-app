import {StyleSheet, View} from 'react-native';
import React, {useEffect} from 'react';
import Header from '../common/header/Header.tsx';

import {observer} from 'mobx-react-lite';
import Loader from '../common/Loader.tsx';
import store from '../../store/intex.ts';
import AdminProductList from './components/AdminProductList.tsx';

const AdminHomeScreen = () => {
  const {isLoading, onGetProducts} = store.productsStore;

  useEffect(() => {
    onGetProducts();
  }, []);

  if (isLoading) {
    return <Loader />;
  }
  return (
    <View style={style.mainContainer}>
      <Header />
      <AdminProductList />
    </View>
  );
};

const style = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#f4f5f7',
    flex: 1,
  },
});

export default observer(AdminHomeScreen);
