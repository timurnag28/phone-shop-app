import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';

import store from '../store/intex.ts';
import {observer} from 'mobx-react-lite';
import Loader from '../screens/common/Loader.tsx';
import CartScreen from '../screens/customer/CartScreen';
import CheckoutScreen from '../screens/customer/CheckoutScreen';
import AdminHomeScreen from '../screens/admin/AdminHomeScreen';
import EditProductScreen from '../screens/admin/EditProductScreen';
import OrdersScreen from '../screens/admin/OrdersScreen.';
import OrderDetailsScreen from '../screens/admin/OrderDetailsScreen';
import SaveFieldsButton from '../screens/admin/components/SaveFieldsButton.tsx';
import HeaderBackButton from '../screens/admin/components/OrdersBackButton.tsx';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {RootStackParamList} from '../inerfaces/interfaces';
import {CUSTOMER} from '../share/consts.ts';
import {ROUTES} from './routes';
import CustomerHomeScreen from '../screens/customer/CustomerHomeScreen';
import AuthScreen from '../screens/authorization/AuthScreen';

const Stack = createNativeStackNavigator<RootStackParamList>();
const GetStack = observer((): any => {
  const {userRole, token, isAuthLoading} = store.authStore;
  const renderSaveButton = () => <SaveFieldsButton />;
  const renderBackButton = () => <HeaderBackButton />;

  useEffect(() => {
    void store.authStore.onGetAuthToken();
  }, []);

  if (isAuthLoading) {
    return <Loader />;
  }

  return (
    <Stack.Navigator>
      {!token ? (
        <Stack.Screen
          name={ROUTES.AUTHORIZATION_SCREEN}
          component={AuthScreen}
          options={{
            headerShown: false,
            animationTypeForReplace: !token ? 'pop' : 'push',
          }}
        />
      ) : userRole === CUSTOMER ? (
        <>
          <Stack.Screen
            name={ROUTES.CUSTOMER_HOME_SCREEN}
            component={CustomerHomeScreen}
            options={{headerShown: false, title: 'Phone list'}}
          />
          <Stack.Screen
            name={ROUTES.CART_SCREEN}
            component={CartScreen}
            options={{title: 'Shopping cart'}}
          />
          <Stack.Screen
            name={ROUTES.CHECKOUT_SCREEN}
            component={CheckoutScreen}
            options={{title: 'Checkout'}}
          />
        </>
      ) : (
        <>
          <Stack.Screen
            name={ROUTES.ADMIN_HOME_SCREEN}
            component={AdminHomeScreen}
            options={{headerShown: false, title: 'Phone list'}}
          />
          <Stack.Screen
            name={ROUTES.EDIT_PRODUCT_SCREEN}
            component={EditProductScreen}
            options={{
              title: 'Product editing',
              headerLeft: renderBackButton,
              headerRight: renderSaveButton,
            }}
          />
          <Stack.Screen
            name={ROUTES.ORDERS_SCREEN}
            component={OrdersScreen}
            options={{
              title: 'Orders',
            }}
          />
          <Stack.Screen
            name={ROUTES.ORDER_DETAILS_SCREEN}
            component={OrderDetailsScreen}
            options={{
              title: 'Order Details',
            }}
          />
        </>
      )}
    </Stack.Navigator>
  );
});
const AppNavigator = () => {
  return (
    <NavigationContainer>
      <GetStack />
    </NavigationContainer>
  );
};

export default AppNavigator;
