import {BaseResponse, ProductItem} from '../inerfaces/interfaces.ts';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {DATA_ERROR, PRODUCTS_STORAGE_KEY} from '../share/consts.ts';
import {productsData} from '../share/Mocks.ts';

let products: ProductItem[] = productsData;

class ProductsService {
  async getProducts(): Promise<BaseResponse<ProductItem[]>> {
    return new Promise(async resolve => {
      setTimeout(async () => {
        const storedProducts = await AsyncStorage.getItem(PRODUCTS_STORAGE_KEY);
        if (storedProducts) {
          products = JSON.parse(storedProducts);
        } else {
          await AsyncStorage.setItem(
            PRODUCTS_STORAGE_KEY,
            JSON.stringify(products),
          );
        }
        resolve({
          status: 200,
          data: products,
        });
      }, 2000);
    });
  }

  updateProduct(newProduct: ProductItem): Promise<BaseResponse<ProductItem>> {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        try {
          const index = products.findIndex(
            product => product.id === newProduct.id,
          );
          products[index] = newProduct;

          await AsyncStorage.setItem(
            PRODUCTS_STORAGE_KEY,
            JSON.stringify(products),
          );
          resolve({status: 200, data: products[index]});
        } catch (e) {
          reject(DATA_ERROR);
        }
      }, 1000);
    });
  }
}

export default ProductsService;
