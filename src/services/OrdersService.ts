import {BaseResponse, Order} from '../inerfaces/interfaces.ts';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {DATA_ERROR, ORDERS_STORAGE_KEY} from '../share/consts.ts';
import {ordersData} from '../share/Mocks.ts';

let orders: Order[] = ordersData;

class OrderService {
  getOrders(): Promise<BaseResponse<Order[]>> {
    return new Promise(resolve => {
      setTimeout(async () => {
        const storedOrders = await AsyncStorage.getItem(ORDERS_STORAGE_KEY);
        if (storedOrders) {
          orders = JSON.parse(storedOrders);
        } else {
          await AsyncStorage.setItem(
            ORDERS_STORAGE_KEY,
            JSON.stringify(orders),
          );
        }
        resolve({status: 200, data: orders});
      }, 2000);
    });
  }

  createOrder(order: Order): Promise<BaseResponse<Order[]>> {
    return new Promise((resolve, reject) => {
      setTimeout(async () => {
        try {
          const storedOrders = await AsyncStorage.getItem(ORDERS_STORAGE_KEY);
          if (storedOrders) {
            orders = JSON.parse(storedOrders);
            let lastElement = orders[orders.length - 1];
            if (lastElement) {
              orders.push({
                orderId: lastElement.orderId + 1,
                customerName: order.customerName,
                customerLastName: order.customerLastName,
                customerEmail: order.customerEmail,
                products: [...order.products],
                totalPrice: order.totalPrice,
              });
            }
          } else {
            orders.push({
              orderId: 1,
              customerName: order.customerName,
              customerLastName: order.customerLastName,
              customerEmail: order.customerEmail,
              products: [...order.products],
              totalPrice: order.totalPrice,
            });
          }
        } catch (e) {
          reject(DATA_ERROR);
        }

        await AsyncStorage.setItem(ORDERS_STORAGE_KEY, JSON.stringify(orders));
        resolve({status: 200, data: orders});
      }, 1000);
    });
  }
}

export default OrderService;
