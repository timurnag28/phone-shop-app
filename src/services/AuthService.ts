import {AuthData, BaseResponse, User} from '../inerfaces/interfaces.ts';
import {users} from '../share/Mocks.ts';
import {INCORRECT_USER} from '../share/consts.ts';

class AuthService {
  login(username: string, password: string): Promise<BaseResponse<AuthData>> {
    return new Promise(resolve => {
      setTimeout(() => {
        const responseResult = users.find(
          (user: User) =>
            user.username === username && user.password === password,
        );
        if (responseResult) {
          const token = 'mock-token';
          resolve({
            status: 200,
            data: {
              token: token,
              role: responseResult?.role,
            },
          });
        } else {
          resolve({
            status: 401,
            data: {errorMessage: INCORRECT_USER},
          });
        }
      }, 2000);
    });
  }
}

export default AuthService;
