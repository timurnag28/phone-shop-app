import AuthService from './AuthService.ts';
import {DATA_ERROR, INCORRECT_USER, NO_DATA} from '../share/consts.ts';
import ProductsService from './ProductsService.ts';
import OrderService from './OrdersService.ts';
import {BaseResponse, Order, ProductItem} from '../inerfaces/interfaces.ts';

export class Api {
  onLogin = (username: string, password: string) => {
    return interceptor(new AuthService().login(username, password));
  };

  onGetProducts = () => {
    return interceptor(new ProductsService().getProducts());
  };

  onUpdateProduct = (newProduct: ProductItem) => {
    return interceptor(new ProductsService().updateProduct(newProduct));
  };

  onCreateOrder = (order: Order) => {
    return interceptor(new OrderService().createOrder(order));
  };

  onGetOrders = () => {
    return interceptor(new OrderService().getOrders());
  };
}

export const interceptor = async <T>(method: Promise<BaseResponse<T>>) => {
  const response = await method;
  switch (response.status) {
    case 200:
      return response;
    case 401: {
      throw new Error(INCORRECT_USER);
    }
    case 404: {
      throw new Error(NO_DATA);
    }
    default: {
      throw new Error(DATA_ERROR);
    }
  }
};
