const {defaults: tsjPreset} = require('ts-jest/presets');

module.exports = {
  ...tsjPreset,
  preset: 'react-native',
  setupFiles: ['<rootDir>/setup-jest.js'],
  transform: {
    '^.+\\.jsx$': 'babel-jest',
    '^.+\\.tsx?$': [
      'ts-jest',
      {
        tsconfig: 'tsconfig.jest.json',
      },
    ],
  },
  // transformIgnorePatterns: [
  //   'node_modules/(?!((jest-)?react-native|react-native-gesture-handler|@react-native-async-storage|react-native-flash-message|mobx-react-lite|@react-native)/)',
  // ],
  testPathIgnorePatterns: [
    'node_modules/(?!(react-native|@react-native|@react-navigation|react-native-screens|react-native-reanimated|react-native-vector-icons|@react-navigation)/)',
  ],
  moduleNameMapper: {
    '^mobx$': '<rootDir>/node_modules/mobx',
    '^mobx-react-lite$': '<rootDir>/node_modules/mobx-react-lite',
    '^mobx-react$': '<rootDir>/node_modules/mobx-react',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};
